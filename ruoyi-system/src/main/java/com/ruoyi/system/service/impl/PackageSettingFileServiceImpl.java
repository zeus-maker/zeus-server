package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PackageSettingFileMapper;
import com.ruoyi.system.domain.PackageSettingFile;
import com.ruoyi.system.service.IPackageSettingFileService;

/**
 * 打包配置文件Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class PackageSettingFileServiceImpl implements IPackageSettingFileService 
{
    @Autowired
    private PackageSettingFileMapper packageSettingFileMapper;

    /**
     * 查询打包配置文件
     * 
     * @param id 打包配置文件主键
     * @return 打包配置文件
     */
    @Override
    public PackageSettingFile selectPackageSettingFileById(Long id)
    {
        return packageSettingFileMapper.selectPackageSettingFileById(id);
    }

    /**
     * 查询打包配置文件列表
     * 
     * @param packageSettingFile 打包配置文件
     * @return 打包配置文件
     */
    @Override
    public List<PackageSettingFile> selectPackageSettingFileList(PackageSettingFile packageSettingFile)
    {
        return packageSettingFileMapper.selectPackageSettingFileList(packageSettingFile);
    }

    /**
     * 新增打包配置文件
     * 
     * @param packageSettingFile 打包配置文件
     * @return 结果
     */
    @Override
    public int insertPackageSettingFile(PackageSettingFile packageSettingFile)
    {
        packageSettingFile.setCreateTime(DateUtils.getNowDate());
        return packageSettingFileMapper.insertPackageSettingFile(packageSettingFile);
    }

    /**
     * 修改打包配置文件
     * 
     * @param packageSettingFile 打包配置文件
     * @return 结果
     */
    @Override
    public int updatePackageSettingFile(PackageSettingFile packageSettingFile)
    {
        packageSettingFile.setUpdateTime(DateUtils.getNowDate());
        return packageSettingFileMapper.updatePackageSettingFile(packageSettingFile);
    }

    /**
     * 批量删除打包配置文件
     * 
     * @param ids 需要删除的打包配置文件主键
     * @return 结果
     */
    @Override
    public int deletePackageSettingFileByIds(Long[] ids)
    {
        return packageSettingFileMapper.deletePackageSettingFileByIds(ids);
    }

    /**
     * 删除打包配置文件信息
     * 
     * @param id 打包配置文件主键
     * @return 结果
     */
    @Override
    public int deletePackageSettingFileById(Long id)
    {
        return packageSettingFileMapper.deletePackageSettingFileById(id);
    }
}
