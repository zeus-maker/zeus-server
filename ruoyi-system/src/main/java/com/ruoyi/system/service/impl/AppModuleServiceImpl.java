package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AppModuleMapper;
import com.ruoyi.system.domain.AppModule;
import com.ruoyi.system.service.IAppModuleService;

/**
 * 模块配置Service业务层处理
 * 
 * @author marion
 * @date 2023-07-12
 */
@Service
public class AppModuleServiceImpl implements IAppModuleService 
{
    @Autowired
    private AppModuleMapper appModuleMapper;

    /**
     * 查询模块配置
     * 
     * @param id 模块配置主键
     * @return 模块配置
     */
    @Override
    public AppModule selectAppModuleById(Long id)
    {
        return appModuleMapper.selectAppModuleById(id);
    }

    /**
     * 查询模块配置列表
     * 
     * @param appModule 模块配置
     * @return 模块配置
     */
    @Override
    public List<AppModule> selectAppModuleList(AppModule appModule)
    {
        return appModuleMapper.selectAppModuleList(appModule);
    }

    /**
     * 新增模块配置
     * 
     * @param appModule 模块配置
     * @return 结果
     */
    @Override
    public int insertAppModule(AppModule appModule)
    {
        appModule.setCreateTime(DateUtils.getNowDate());
        return appModuleMapper.insertAppModule(appModule);
    }

    /**
     * 修改模块配置
     * 
     * @param appModule 模块配置
     * @return 结果
     */
    @Override
    public int updateAppModule(AppModule appModule)
    {
        appModule.setUpdateTime(DateUtils.getNowDate());
        return appModuleMapper.updateAppModule(appModule);
    }

    /**
     * 批量删除模块配置
     * 
     * @param ids 需要删除的模块配置主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleByIds(Long[] ids)
    {
        return appModuleMapper.deleteAppModuleByIds(ids);
    }

    /**
     * 删除模块配置信息
     * 
     * @param id 模块配置主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleById(Long id)
    {
        return appModuleMapper.deleteAppModuleById(id);
    }
}
