package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AdminUserModuleMapper;
import com.ruoyi.system.domain.AdminUserModule;
import com.ruoyi.system.service.IAdminUserModuleService;

/**
 * 用户模块配置Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-10
 */
@Service
public class AdminUserModuleServiceImpl implements IAdminUserModuleService 
{
    @Autowired
    private AdminUserModuleMapper adminUserModuleMapper;

    /**
     * 查询用户模块配置
     * 
     * @param id 用户模块配置主键
     * @return 用户模块配置
     */
    @Override
    public AdminUserModule selectAdminUserModuleById(Long id)
    {
        return adminUserModuleMapper.selectAdminUserModuleById(id);
    }

    /**
     * 查询用户模块配置列表
     * 
     * @param adminUserModule 用户模块配置
     * @return 用户模块配置
     */
    @Override
    public List<AdminUserModule> selectAdminUserModuleList(AdminUserModule adminUserModule)
    {
        return adminUserModuleMapper.selectAdminUserModuleList(adminUserModule);
    }

    /**
     * 新增用户模块配置
     * 
     * @param adminUserModule 用户模块配置
     * @return 结果
     */
    @Override
    public int insertAdminUserModule(AdminUserModule adminUserModule)
    {
        adminUserModule.setCreateTime(DateUtils.getNowDate());
        return adminUserModuleMapper.insertAdminUserModule(adminUserModule);
    }

    /**
     * 修改用户模块配置
     * 
     * @param adminUserModule 用户模块配置
     * @return 结果
     */
    @Override
    public int updateAdminUserModule(AdminUserModule adminUserModule)
    {
        adminUserModule.setUpdateTime(DateUtils.getNowDate());
        return adminUserModuleMapper.updateAdminUserModule(adminUserModule);
    }

    /**
     * 批量删除用户模块配置
     * 
     * @param ids 需要删除的用户模块配置主键
     * @return 结果
     */
    @Override
    public int deleteAdminUserModuleByIds(Long[] ids)
    {
        return adminUserModuleMapper.deleteAdminUserModuleByIds(ids);
    }

    /**
     * 删除用户模块配置信息
     * 
     * @param id 用户模块配置主键
     * @return 结果
     */
    @Override
    public int deleteAdminUserModuleById(Long id)
    {
        return adminUserModuleMapper.deleteAdminUserModuleById(id);
    }
}
