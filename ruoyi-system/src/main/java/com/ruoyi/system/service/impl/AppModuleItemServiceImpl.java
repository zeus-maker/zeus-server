package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AppModuleItemMapper;
import com.ruoyi.system.domain.AppModuleItem;
import com.ruoyi.system.service.IAppModuleItemService;

/**
 * 模块配置项Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class AppModuleItemServiceImpl implements IAppModuleItemService 
{
    @Autowired
    private AppModuleItemMapper appModuleItemMapper;

    /**
     * 查询模块配置项
     * 
     * @param id 模块配置项主键
     * @return 模块配置项
     */
    @Override
    public AppModuleItem selectAppModuleItemById(Long id)
    {
        return appModuleItemMapper.selectAppModuleItemById(id);
    }

    /**
     * 查询模块配置项列表
     * 
     * @param appModuleItem 模块配置项
     * @return 模块配置项
     */
    @Override
    public List<AppModuleItem> selectAppModuleItemList(AppModuleItem appModuleItem)
    {
        return appModuleItemMapper.selectAppModuleItemList(appModuleItem);
    }

    /**
     * 新增模块配置项
     * 
     * @param appModuleItem 模块配置项
     * @return 结果
     */
    @Override
    public int insertAppModuleItem(AppModuleItem appModuleItem)
    {
        appModuleItem.setCreateTime(DateUtils.getNowDate());
        return appModuleItemMapper.insertAppModuleItem(appModuleItem);
    }

    /**
     * 修改模块配置项
     * 
     * @param appModuleItem 模块配置项
     * @return 结果
     */
    @Override
    public int updateAppModuleItem(AppModuleItem appModuleItem)
    {
        appModuleItem.setUpdateTime(DateUtils.getNowDate());
        return appModuleItemMapper.updateAppModuleItem(appModuleItem);
    }

    /**
     * 批量删除模块配置项
     * 
     * @param ids 需要删除的模块配置项主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleItemByIds(Long[] ids)
    {
        return appModuleItemMapper.deleteAppModuleItemByIds(ids);
    }

    /**
     * 删除模块配置项信息
     * 
     * @param id 模块配置项主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleItemById(Long id)
    {
        return appModuleItemMapper.deleteAppModuleItemById(id);
    }
}
