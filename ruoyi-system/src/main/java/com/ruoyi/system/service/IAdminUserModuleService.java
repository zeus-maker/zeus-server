package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AdminUserModule;

/**
 * 用户模块配置Service接口
 * 
 * @author Marion
 * @date 2023-07-10
 */
public interface IAdminUserModuleService 
{
    /**
     * 查询用户模块配置
     * 
     * @param id 用户模块配置主键
     * @return 用户模块配置
     */
    public AdminUserModule selectAdminUserModuleById(Long id);

    /**
     * 查询用户模块配置列表
     * 
     * @param adminUserModule 用户模块配置
     * @return 用户模块配置集合
     */
    public List<AdminUserModule> selectAdminUserModuleList(AdminUserModule adminUserModule);

    /**
     * 新增用户模块配置
     * 
     * @param adminUserModule 用户模块配置
     * @return 结果
     */
    public int insertAdminUserModule(AdminUserModule adminUserModule);

    /**
     * 修改用户模块配置
     * 
     * @param adminUserModule 用户模块配置
     * @return 结果
     */
    public int updateAdminUserModule(AdminUserModule adminUserModule);

    /**
     * 批量删除用户模块配置
     * 
     * @param ids 需要删除的用户模块配置主键集合
     * @return 结果
     */
    public int deleteAdminUserModuleByIds(Long[] ids);

    /**
     * 删除用户模块配置信息
     * 
     * @param id 用户模块配置主键
     * @return 结果
     */
    public int deleteAdminUserModuleById(Long id);
}
