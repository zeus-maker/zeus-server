package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AppModuleCateMapper;
import com.ruoyi.system.domain.AppModuleCate;
import com.ruoyi.system.service.IAppModuleCateService;

/**
 * APP组件分类Service业务层处理
 * 
 * @author marion
 * @date 2023-07-09
 */
@Service
public class AppModuleCateServiceImpl implements IAppModuleCateService 
{
    @Autowired
    private AppModuleCateMapper appModuleCateMapper;

    /**
     * 查询APP组件分类
     * 
     * @param id APP组件分类主键
     * @return APP组件分类
     */
    @Override
    public AppModuleCate selectAppModuleCateById(Long id)
    {
        return appModuleCateMapper.selectAppModuleCateById(id);
    }

    /**
     * 查询APP组件分类列表
     * 
     * @param appModuleCate APP组件分类
     * @return APP组件分类
     */
    @Override
    public List<AppModuleCate> selectAppModuleCateList(AppModuleCate appModuleCate)
    {
        return appModuleCateMapper.selectAppModuleCateList(appModuleCate);
    }

    /**
     * 新增APP组件分类
     * 
     * @param appModuleCate APP组件分类
     * @return 结果
     */
    @Override
    public int insertAppModuleCate(AppModuleCate appModuleCate)
    {
        appModuleCate.setCreateTime(DateUtils.getNowDate());
        return appModuleCateMapper.insertAppModuleCate(appModuleCate);
    }

    /**
     * 修改APP组件分类
     * 
     * @param appModuleCate APP组件分类
     * @return 结果
     */
    @Override
    public int updateAppModuleCate(AppModuleCate appModuleCate)
    {
        appModuleCate.setUpdateTime(DateUtils.getNowDate());
        return appModuleCateMapper.updateAppModuleCate(appModuleCate);
    }

    /**
     * 批量删除APP组件分类
     * 
     * @param ids 需要删除的APP组件分类主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleCateByIds(Long[] ids)
    {
        return appModuleCateMapper.deleteAppModuleCateByIds(ids);
    }

    /**
     * 删除APP组件分类信息
     * 
     * @param id APP组件分类主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleCateById(Long id)
    {
        return appModuleCateMapper.deleteAppModuleCateById(id);
    }
}
