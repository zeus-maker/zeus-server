package com.ruoyi.system.service;

import java.util.Map;

public interface JenkinsService {

    /**
     * 获取Job
     */
    Object getJob(String jobName);

    /**
     * 构建项目
     */
    Boolean build(String jobName, Map<String, String> param);

    /**
     * 获得构建结果
     */
    Object getBuildInfo(String jobName);
}
