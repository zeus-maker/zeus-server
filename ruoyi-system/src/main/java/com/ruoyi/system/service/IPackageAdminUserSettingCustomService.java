package com.ruoyi.system.service;

/**
 * 用户打包设置Service接口
 *
 * @author Marion
 * @date 2023-07-10
 */
public interface IPackageAdminUserSettingCustomService
{
    /**
     * 获得打包配置信息
     *
     * @param id 用户打包配置ID
     * @return bool
     */
    boolean sitemap(Long id);

    /**
     * 开始打包
     *
     * @param id 用户打包设置主键
     * @return bool
     */
    boolean buildPackage(Long id);

}
