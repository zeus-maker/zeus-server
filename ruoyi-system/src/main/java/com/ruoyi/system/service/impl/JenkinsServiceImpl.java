package com.ruoyi.system.service.impl;

import com.ruoyi.system.service.JenkinsService;
import com.ruoyi.system.util.jenkins.JobApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JenkinsServiceImpl implements JenkinsService {

    @Autowired
    private JobApi jobApi;

    @Override
    public Object getJob(String jobName) {
        jobApi.getJob(jobName);
        return true;
    }

    @Override
    public Boolean build(String jobName, Map<String, String> param) {
        jobApi.buildParamJob(jobName, param);
        return true;
    }

    @Override
    public Object getBuildInfo(String jobName) {
        jobApi.getJob(jobName);
        return true;
    }
}
