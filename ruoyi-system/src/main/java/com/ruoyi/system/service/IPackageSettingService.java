package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PackageSetting;

/**
 * 打包配置Service接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface IPackageSettingService 
{
    /**
     * 查询打包配置
     * 
     * @param id 打包配置主键
     * @return 打包配置
     */
    public PackageSetting selectPackageSettingById(Long id);

    /**
     * 查询打包配置列表
     * 
     * @param packageSetting 打包配置
     * @return 打包配置集合
     */
    public List<PackageSetting> selectPackageSettingList(PackageSetting packageSetting);

    /**
     * 新增打包配置
     * 
     * @param packageSetting 打包配置
     * @return 结果
     */
    public int insertPackageSetting(PackageSetting packageSetting);

    /**
     * 修改打包配置
     * 
     * @param packageSetting 打包配置
     * @return 结果
     */
    public int updatePackageSetting(PackageSetting packageSetting);

    /**
     * 批量删除打包配置
     * 
     * @param ids 需要删除的打包配置主键集合
     * @return 结果
     */
    public int deletePackageSettingByIds(Long[] ids);

    /**
     * 删除打包配置信息
     * 
     * @param id 打包配置主键
     * @return 结果
     */
    public int deletePackageSettingById(Long id);
}
