package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AppModuleAdminUserMapper;
import com.ruoyi.system.domain.AppModuleAdminUser;
import com.ruoyi.system.service.IAppModuleAdminUserService;

/**
 * 用户-APP模块配置项（管理用户的各项配置）Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class AppModuleAdminUserServiceImpl implements IAppModuleAdminUserService 
{
    @Autowired
    private AppModuleAdminUserMapper appModuleAdminUserMapper;

    /**
     * 查询用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param id 用户-APP模块配置项（管理用户的各项配置）主键
     * @return 用户-APP模块配置项（管理用户的各项配置）
     */
    @Override
    public AppModuleAdminUser selectAppModuleAdminUserById(Long id)
    {
        return appModuleAdminUserMapper.selectAppModuleAdminUserById(id);
    }

    /**
     * 查询用户-APP模块配置项（管理用户的各项配置）列表
     * 
     * @param appModuleAdminUser 用户-APP模块配置项（管理用户的各项配置）
     * @return 用户-APP模块配置项（管理用户的各项配置）
     */
    @Override
    public List<AppModuleAdminUser> selectAppModuleAdminUserList(AppModuleAdminUser appModuleAdminUser)
    {
        return appModuleAdminUserMapper.selectAppModuleAdminUserList(appModuleAdminUser);
    }

    /**
     * 新增用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param appModuleAdminUser 用户-APP模块配置项（管理用户的各项配置）
     * @return 结果
     */
    @Override
    public int insertAppModuleAdminUser(AppModuleAdminUser appModuleAdminUser)
    {
        appModuleAdminUser.setCreateTime(DateUtils.getNowDate());
        return appModuleAdminUserMapper.insertAppModuleAdminUser(appModuleAdminUser);
    }

    /**
     * 修改用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param appModuleAdminUser 用户-APP模块配置项（管理用户的各项配置）
     * @return 结果
     */
    @Override
    public int updateAppModuleAdminUser(AppModuleAdminUser appModuleAdminUser)
    {
        appModuleAdminUser.setUpdateTime(DateUtils.getNowDate());
        return appModuleAdminUserMapper.updateAppModuleAdminUser(appModuleAdminUser);
    }

    /**
     * 批量删除用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param ids 需要删除的用户-APP模块配置项（管理用户的各项配置）主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleAdminUserByIds(Long[] ids)
    {
        return appModuleAdminUserMapper.deleteAppModuleAdminUserByIds(ids);
    }

    /**
     * 删除用户-APP模块配置项（管理用户的各项配置）信息
     * 
     * @param id 用户-APP模块配置项（管理用户的各项配置）主键
     * @return 结果
     */
    @Override
    public int deleteAppModuleAdminUserById(Long id)
    {
        return appModuleAdminUserMapper.deleteAppModuleAdminUserById(id);
    }
}
