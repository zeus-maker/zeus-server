package com.ruoyi.system.service.impl;

import com.google.common.collect.Maps;
import com.ruoyi.system.domain.Apps;
import com.ruoyi.system.domain.PackageAdminUserSetting;
import com.ruoyi.system.mapper.PackageAdminUserSettingMapper;
import com.ruoyi.system.service.IAppsService;
import com.ruoyi.system.service.IPackageAdminUserSettingCustomService;
import com.ruoyi.system.service.JenkinsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 用户打包设置Service业务层处理
 *
 * @author Marion
 * @date 2023-07-10
 */
@Service
public class PackageAdminUserSettingCustomServiceImpl implements IPackageAdminUserSettingCustomService {
    @Autowired
    private PackageAdminUserSettingMapper packageAdminUserSettingMapper;

    @Autowired
    private IAppsService appsService;

    @Autowired
    private JenkinsService jenkinsService;

    @Override
    public boolean sitemap(Long id) {
        /**
         *
         */
        return false;
    }

    @Override
    public boolean buildPackage(Long id) {
        /**
         * 1. 根据ID查询打包信息
         * 2. 查询打包APP的网址
         * 3. 请求打包Jenkins
         */
        PackageAdminUserSetting packageAdminUserSetting = packageAdminUserSettingMapper.selectPackageAdminUserSettingById(id);
        Apps apps = appsService.selectAppsById(packageAdminUserSetting.getAppId());
        Map<String, String> param = Maps.newHashMap();
        param.put("ENTRANCE_URL", apps.getUrl());
        param.put("PACK_ID", id.toString());
        jenkinsService.build("zeus-andriod", param);
        return false;
    }
}
