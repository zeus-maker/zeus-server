package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AppModule;

/**
 * 模块配置Service接口
 * 
 * @author marion
 * @date 2023-07-12
 */
public interface IAppModuleService 
{
    /**
     * 查询模块配置
     * 
     * @param id 模块配置主键
     * @return 模块配置
     */
    public AppModule selectAppModuleById(Long id);

    /**
     * 查询模块配置列表
     * 
     * @param appModule 模块配置
     * @return 模块配置集合
     */
    public List<AppModule> selectAppModuleList(AppModule appModule);

    /**
     * 新增模块配置
     * 
     * @param appModule 模块配置
     * @return 结果
     */
    public int insertAppModule(AppModule appModule);

    /**
     * 修改模块配置
     * 
     * @param appModule 模块配置
     * @return 结果
     */
    public int updateAppModule(AppModule appModule);

    /**
     * 批量删除模块配置
     * 
     * @param ids 需要删除的模块配置主键集合
     * @return 结果
     */
    public int deleteAppModuleByIds(Long[] ids);

    /**
     * 删除模块配置信息
     * 
     * @param id 模块配置主键
     * @return 结果
     */
    public int deleteAppModuleById(Long id);
}
