package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模块配置项对象 t_app_module_item
 * 
 * @author Marion
 * @date 2023-07-12
 */
public class AppModuleItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    @Excel(name = "主键ID")
    private Long id;

    /** 模块ID */
    @Excel(name = "模块ID")
    private Long moduleId;

    /** 配置类型 0-文本输入框 1-select 2-radio单选框 3-checkbox多选框 4-file */
    @Excel(name = "配置类型 0-文本输入框 1-select 2-radio单选框 3-checkbox多选框 4-file")
    private Long type;

    /** 配置项名称 */
    @Excel(name = "配置项名称")
    private String itemName;

    /** 配置项字段 */
    @Excel(name = "配置项字段")
    private String itemKey;

    /** 配置项默认值, 单选多选JSON格式 */
    @Excel(name = "配置项默认值, 单选多选JSON格式")
    private String itemValue;

    /** 是否软删除 0-否 1-是 */
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setModuleId(Long moduleId) 
    {
        this.moduleId = moduleId;
    }

    public Long getModuleId() 
    {
        return moduleId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setItemName(String itemName) 
    {
        this.itemName = itemName;
    }

    public String getItemName() 
    {
        return itemName;
    }
    public void setItemKey(String itemKey) 
    {
        this.itemKey = itemKey;
    }

    public String getItemKey() 
    {
        return itemKey;
    }
    public void setItemValue(String itemValue) 
    {
        this.itemValue = itemValue;
    }

    public String getItemValue() 
    {
        return itemValue;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("moduleId", getModuleId())
            .append("type", getType())
            .append("itemName", getItemName())
            .append("itemKey", getItemKey())
            .append("itemValue", getItemValue())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
