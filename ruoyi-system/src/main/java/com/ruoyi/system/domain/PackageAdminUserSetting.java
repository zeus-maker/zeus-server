package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户打包设置对象 t_package_admin_user_setting
 * 
 * @author Marion
 * @date 2023-07-12
 */
public class PackageAdminUserSetting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    @Excel(name = "主键ID")
    private Long id;

    /** 管理用户UID */
    @Excel(name = "管理用户UID")
    private Long uid;

    /** 应用APP_ID */
    @Excel(name = "应用APP_ID")
    private Long appId;

    /** 版本号 */
    @Excel(name = "版本号")
    private Long appVersion;

    /** 版本名 */
    @Excel(name = "版本名")
    private String appVersionName;

    /** 平台 1-安卓 2-苹果 */
    @Excel(name = "平台 1-安卓 2-苹果")
    private Integer platform;

    /** 包名 */
    @Excel(name = "包名")
    private String packageName;

    /** 文件包类型 1-apk 2-aab */
    @Excel(name = "文件包类型 1-apk 2-aab")
    private Integer packageType;

    /** 签名文件 */
    @Excel(name = "签名文件")
    private String signFile;

    /** 签名文件密码 */
    @Excel(name = "签名文件密码")
    private String signFilePwd;

    /** 签名别名 */
    @Excel(name = "签名别名")
    private String signAlias;

    /** 签名别名密码 */
    @Excel(name = "签名别名密码")
    private String signAliasPwd;

    /** 是否软删除 0-否 1-是 */
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setAppId(Long appId) 
    {
        this.appId = appId;
    }

    public Long getAppId() 
    {
        return appId;
    }
    public void setAppVersion(Long appVersion) 
    {
        this.appVersion = appVersion;
    }

    public Long getAppVersion() 
    {
        return appVersion;
    }
    public void setAppVersionName(String appVersionName) 
    {
        this.appVersionName = appVersionName;
    }

    public String getAppVersionName() 
    {
        return appVersionName;
    }
    public void setPlatform(Integer platform) 
    {
        this.platform = platform;
    }

    public Integer getPlatform() 
    {
        return platform;
    }
    public void setPackageName(String packageName) 
    {
        this.packageName = packageName;
    }

    public String getPackageName() 
    {
        return packageName;
    }
    public void setPackageType(Integer packageType) 
    {
        this.packageType = packageType;
    }

    public Integer getPackageType() 
    {
        return packageType;
    }
    public void setSignFile(String signFile) 
    {
        this.signFile = signFile;
    }

    public String getSignFile() 
    {
        return signFile;
    }
    public void setSignFilePwd(String signFilePwd) 
    {
        this.signFilePwd = signFilePwd;
    }

    public String getSignFilePwd() 
    {
        return signFilePwd;
    }
    public void setSignAlias(String signAlias) 
    {
        this.signAlias = signAlias;
    }

    public String getSignAlias() 
    {
        return signAlias;
    }
    public void setSignAliasPwd(String signAliasPwd) 
    {
        this.signAliasPwd = signAliasPwd;
    }

    public String getSignAliasPwd() 
    {
        return signAliasPwd;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("uid", getUid())
            .append("appId", getAppId())
            .append("appVersion", getAppVersion())
            .append("appVersionName", getAppVersionName())
            .append("platform", getPlatform())
            .append("packageName", getPackageName())
            .append("packageType", getPackageType())
            .append("signFile", getSignFile())
            .append("signFilePwd", getSignFilePwd())
            .append("signAlias", getSignAlias())
            .append("signAliasPwd", getSignAliasPwd())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
