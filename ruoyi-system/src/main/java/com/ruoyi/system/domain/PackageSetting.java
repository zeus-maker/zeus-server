package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 打包配置对象 t_package_setting
 * 
 * @author Marion
 * @date 2023-07-12
 */
public class PackageSetting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    @Excel(name = "主键ID")
    private Long id;

    /** 平台 1-安卓 2-苹果 */
    @Excel(name = "平台 1-安卓 2-苹果")
    private Integer platform;

    /** 配置项ID */
    @Excel(name = "配置项ID")
    private String metaKey;

    /** 默认值 */
    @Excel(name = "默认值")
    private String metaValue;

    /** 是否软删除 0-否 1-是 */
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlatform(Integer platform) 
    {
        this.platform = platform;
    }

    public Integer getPlatform() 
    {
        return platform;
    }
    public void setMetaKey(String metaKey) 
    {
        this.metaKey = metaKey;
    }

    public String getMetaKey() 
    {
        return metaKey;
    }
    public void setMetaValue(String metaValue) 
    {
        this.metaValue = metaValue;
    }

    public String getMetaValue() 
    {
        return metaValue;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("platform", getPlatform())
            .append("metaKey", getMetaKey())
            .append("metaValue", getMetaValue())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
