package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户模块配置对象 t_admin_user_module
 * 
 * @author Marion
 * @date 2023-07-10
 */
public class AdminUserModule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    @Excel(name = "主键ID")
    private Long id;

    /** 管理用户UID */
    @Excel(name = "管理用户UID")
    private Long uid;

    /** 应用APP_ID */
    @Excel(name = "应用APP_ID")
    private Long appId;

    /** 配置项ID */
    @Excel(name = "配置项ID")
    private Long moduleItemId;

    /** 配置项值（多值用JSON存储） */
    @Excel(name = "配置项值", readConverterExp = "多=值用JSON存储")
    private String moduleItemValue;

    /** 是否软删除 0-否 1-是 */
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setAppId(Long appId) 
    {
        this.appId = appId;
    }

    public Long getAppId() 
    {
        return appId;
    }
    public void setModuleItemId(Long moduleItemId) 
    {
        this.moduleItemId = moduleItemId;
    }

    public Long getModuleItemId() 
    {
        return moduleItemId;
    }
    public void setModuleItemValue(String moduleItemValue) 
    {
        this.moduleItemValue = moduleItemValue;
    }

    public String getModuleItemValue() 
    {
        return moduleItemValue;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("uid", getUid())
            .append("appId", getAppId())
            .append("moduleItemId", getModuleItemId())
            .append("moduleItemValue", getModuleItemValue())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
