package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PackageSettingFile;

/**
 * 打包配置文件Mapper接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface PackageSettingFileMapper 
{
    /**
     * 查询打包配置文件
     * 
     * @param id 打包配置文件主键
     * @return 打包配置文件
     */
    public PackageSettingFile selectPackageSettingFileById(Long id);

    /**
     * 查询打包配置文件列表
     * 
     * @param packageSettingFile 打包配置文件
     * @return 打包配置文件集合
     */
    public List<PackageSettingFile> selectPackageSettingFileList(PackageSettingFile packageSettingFile);

    /**
     * 新增打包配置文件
     * 
     * @param packageSettingFile 打包配置文件
     * @return 结果
     */
    public int insertPackageSettingFile(PackageSettingFile packageSettingFile);

    /**
     * 修改打包配置文件
     * 
     * @param packageSettingFile 打包配置文件
     * @return 结果
     */
    public int updatePackageSettingFile(PackageSettingFile packageSettingFile);

    /**
     * 删除打包配置文件
     * 
     * @param id 打包配置文件主键
     * @return 结果
     */
    public int deletePackageSettingFileById(Long id);

    /**
     * 批量删除打包配置文件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePackageSettingFileByIds(Long[] ids);
}
