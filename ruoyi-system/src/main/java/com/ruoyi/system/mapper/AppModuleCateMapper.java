package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AppModuleCate;

/**
 * APP组件分类Mapper接口
 * 
 * @author marion
 * @date 2023-07-09
 */
public interface AppModuleCateMapper 
{
    /**
     * 查询APP组件分类
     * 
     * @param id APP组件分类主键
     * @return APP组件分类
     */
    public AppModuleCate selectAppModuleCateById(Long id);

    /**
     * 查询APP组件分类列表
     * 
     * @param appModuleCate APP组件分类
     * @return APP组件分类集合
     */
    public List<AppModuleCate> selectAppModuleCateList(AppModuleCate appModuleCate);

    /**
     * 新增APP组件分类
     * 
     * @param appModuleCate APP组件分类
     * @return 结果
     */
    public int insertAppModuleCate(AppModuleCate appModuleCate);

    /**
     * 修改APP组件分类
     * 
     * @param appModuleCate APP组件分类
     * @return 结果
     */
    public int updateAppModuleCate(AppModuleCate appModuleCate);

    /**
     * 删除APP组件分类
     * 
     * @param id APP组件分类主键
     * @return 结果
     */
    public int deleteAppModuleCateById(Long id);

    /**
     * 批量删除APP组件分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAppModuleCateByIds(Long[] ids);
}
