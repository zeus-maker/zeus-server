package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PackageAdminUserSettingHistory;

/**
 * 打包历史记录Mapper接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface PackageAdminUserSettingHistoryMapper 
{
    /**
     * 查询打包历史记录
     * 
     * @param id 打包历史记录主键
     * @return 打包历史记录
     */
    public PackageAdminUserSettingHistory selectPackageAdminUserSettingHistoryById(Long id);

    /**
     * 查询打包历史记录列表
     * 
     * @param packageAdminUserSettingHistory 打包历史记录
     * @return 打包历史记录集合
     */
    public List<PackageAdminUserSettingHistory> selectPackageAdminUserSettingHistoryList(PackageAdminUserSettingHistory packageAdminUserSettingHistory);

    /**
     * 新增打包历史记录
     * 
     * @param packageAdminUserSettingHistory 打包历史记录
     * @return 结果
     */
    public int insertPackageAdminUserSettingHistory(PackageAdminUserSettingHistory packageAdminUserSettingHistory);

    /**
     * 修改打包历史记录
     * 
     * @param packageAdminUserSettingHistory 打包历史记录
     * @return 结果
     */
    public int updatePackageAdminUserSettingHistory(PackageAdminUserSettingHistory packageAdminUserSettingHistory);

    /**
     * 删除打包历史记录
     * 
     * @param id 打包历史记录主键
     * @return 结果
     */
    public int deletePackageAdminUserSettingHistoryById(Long id);

    /**
     * 批量删除打包历史记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePackageAdminUserSettingHistoryByIds(Long[] ids);
}
