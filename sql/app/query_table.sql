-- SQL: 查询用户(启动图)设置过的配置项[{"item_field":"module_item_value"}, {"item_field":"module_item_value"}]
select t1.`module_item_id`, t2.`item_key`, t1.`module_item_value`
from t_admin_user_module as `t1`
         left join `t_app_module_item` as t2 on t1.module_item_id = t2.id
where uid = 1
  and t2.module_id = 1;
