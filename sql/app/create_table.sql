-- 1. APP应用表
DROP TABLE IF EXISTS `t_apps`;
CREATE TABLE `t_apps`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `uid`         bigint(20)   NOT NULL comment 'UID',
    `name`        varchar(255) not null default '' comment '应用名称',
    `description` varchar(255) not null default '' comment '应用描述',
    `url`         varchar(255) not null default '' comment '打包网址',
    `create_time` timestamp             default current_timestamp comment '创建时间',
    `update_time` timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE,
    KEY idx_uid (`uid`)
) ENGINE = InnoDB COMMENT = 'APP应用表';

-- 【待定】2. 打包用户表->用于登陆打包平台的账号
DROP TABLE IF EXISTS `t_admin_user`;
CREATE TABLE `t_admin_user`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `name`        varchar(64)  not null default '' comment '名称',
    `account`     varchar(64)  not null default '' comment '账号名',
    `password`    varchar(255) not null default '' comment '密码',
    `create_time` timestamp             default current_timestamp comment '创建时间',
    `update_time` timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '打包用户表';

-- 【待定】3. 管理用户绑定的APP
DROP TABLE IF EXISTS `t_admin_user_app`;
CREATE TABLE `t_admin_user_app`
(
    `id`          int(11)    NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `uid`         bigint(20) NOT NULL COMMENT '管理用户UID',
    `app_id`      int(11)    not null default 0 comment '应用APP ID',
    `create_time` timestamp           default current_timestamp comment '创建时间',
    `update_time` timestamp           default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3) not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '管理用户绑定的APP表';

-- 5. APP模块（定义各项组件启动图、引导页、自定义菜单、版本更新、分享、支付）
DROP TABLE IF EXISTS `t_app_module`;
CREATE TABLE `t_app_module`
(
    `id`          int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `pid`         int(11)       NOT NULL COMMENT '父ID',
    `name`        varchar(64)   not null default '' comment '组件名称',
    `sketch`      varchar(255)  not null default '' comment '组件简述',
    `description` varchar(1000) not null default '' comment '组件说明',
    `create_time` timestamp              default current_timestamp comment '创建时间',
    `update_time` timestamp              default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3)    not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = 'APP模块（定义各项组件启动图、引导页、自定义菜单、版本更新、分享、支付）';

-- 6. APP模块->APP模块配置项
DROP TABLE IF EXISTS `t_app_module_item`;
CREATE TABLE `t_app_module_item`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `module_id`   int(11)      NOT NULL COMMENT '模块ID',
    `type`        int(11)      NOT NULL default 0 COMMENT '配置类型 0-文本输入框 1-select 2-radio单选框 3-checkbox多选框 4-file',
    `item_name`   varchar(255) not null default '' comment '配置项名称',
    `item_key`    varchar(64)  not null default '' comment '配置项字段',
    `item_value`  varchar(255) not null default '' comment '配置项默认值, 单选多选JSON格式',
    `create_time` timestamp             default current_timestamp comment '创建时间',
    `update_time` timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = 'APP模块-APP模块配置项';

-- 7. 用户->APP模块配置项（管理用户的各项配置）
DROP TABLE IF EXISTS `t_app_module_admin_user`;
CREATE TABLE `t_app_module_admin_user`
(
    `id`                int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `uid`               bigint(20)   not null comment '管理用户UID',
    `app_id`            int(11)      not null comment '应用APP_ID',
    `module_item_id`    int(11)      not null comment '配置项ID',
    `module_item_value` varchar(255) not null default '' comment '配置项值（多值用JSON存储）',
    `create_time`       timestamp             default current_timestamp comment '创建时间',
    `update_time`       timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`        tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE,
    KEY idx_uid (`uid`)
) ENGINE = InnoDB COMMENT = '用户-APP模块配置项（管理用户的各项配置）表';

-- 8. 打包配置表
DROP TABLE IF EXISTS `t_package_setting`;
CREATE TABLE `t_package_setting`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `platform`    tinyint(3)   NOT NULL default 0 COMMENT '平台 1-安卓 2-苹果',
    `meta_key`    varchar(255) not null default '' comment '配置项ID',
    `meta_value`  varchar(255) not null default '' comment '默认值',
    `create_time` timestamp             default current_timestamp comment '创建时间',
    `update_time` timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '打包配置表';

-- 11. 用户打包设置表
DROP TABLE IF EXISTS `t_package_admin_user_setting`;
CREATE TABLE `t_package_admin_user_setting`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `uid`              bigint(20)   not null comment '管理用户UID',
    `app_id`           int(11)      not null comment '应用APP_ID',
    `app_version`      bigint(20)   NOT NULL COMMENT '版本号',
    `app_version_name` varchar(255) NOT NULL COMMENT '版本名',
    `platform`         tinyint(3)   NOT NULL default 0 COMMENT '平台 1-安卓 2-苹果',
    `package_name`     varchar(255) NOT NULL COMMENT '包名',
    `package_type`     tinyint(3)   NOT NULL default 0 COMMENT '文件包类型 1-apk 2-aab',
    `sign_file`        varchar(255) NOT NULL default '' COMMENT '签名文件',
    `sign_file_pwd`    varchar(255) NOT NULL default '' COMMENT '签名文件密码',
    `sign_alias`       varchar(255) NOT NULL default '' COMMENT '签名别名',
    `sign_alias_pwd`   varchar(255) NOT NULL default '' COMMENT '签名别名密码',
    `create_time`      timestamp             default current_timestamp comment '创建时间',
    `update_time`      timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`       tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE,
    KEY idx_uid (`uid`)
) ENGINE = InnoDB COMMENT = '用户打包设置表';

-- 用户打包历史记录表
DROP TABLE IF EXISTS `t_package_admin_user_setting_history`;
CREATE TABLE `t_package_admin_user_setting_history`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `uid`              bigint(20)   not null comment '管理用户UID',
    `app_id`           int(11)      not null comment '应用APP_ID',
    `app_version`      bigint(20)   NOT NULL COMMENT '版本号',
    `app_version_name` varchar(255) NOT NULL COMMENT '版本名',
    `platform`         tinyint(3)   NOT NULL default 0 COMMENT '平台 1-安卓 2-苹果',
    `package_name`     varchar(255) NOT NULL COMMENT '包名',
    `package_type`     tinyint(3)   NOT NULL default 0 COMMENT '文件包类型 1-apk 2-aab',
    `module_detail`    text COMMENT '打包模块配置信息',
    `download_url`     varchar(255) NOT NULL default '' COMMENT '下载地址',
    `create_time`      timestamp             default current_timestamp comment '创建时间',
    `update_time`      timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`       tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '用户打包历史记录表';

-- 10. 打包配置文件表
DROP TABLE IF EXISTS `t_package_setting_file`;
CREATE TABLE `t_package_setting_file`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `type`        varchar(255) NOT NULL COMMENT '业务类型 1-IOS签名证书 2-IOS描述文件 3-安卓签名文件 4-IOS配置文件',
    `biz_id`      bigint(20)   NOT NULL default 0 COMMENT '业务主键ID',
    `mime_type`   varchar(255) NOT NULL COMMENT '文件类型',
    `file_size`   bigint(20)   NOT NULL COMMENT '文件大小',
    `file_name`   varchar(255) NOT NULL COMMENT '文件名',
    `create_time` timestamp             default current_timestamp comment '创建时间',
    `update_time` timestamp             default current_timestamp on update current_timestamp comment '更新时间',
    `is_deleted`  tinyint(3)   not null default 0 comment '是否软删除 0-否 1-是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '打包配置文件表';
