-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户打包设置', '2028', '1', 'pack_user_setting', 'pack/pack_user_setting/index', 1, 0, 'C', '0', '0', 'pack:pack_user_setting:list', '#', 'admin', sysdate(), '', null, '用户打包设置菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户打包设置查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_setting:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户打包设置新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_setting:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户打包设置修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_setting:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户打包设置删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_setting:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户打包设置导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_setting:export',       '#', 'admin', sysdate(), '', null, '');