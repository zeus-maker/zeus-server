-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('APP应用', '2021', '1', 'apps', 'system/apps/index', 1, 0, 'C', '0', '0', 'system:apps:list', '#', 'admin', sysdate(), '', null, 'APP应用菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('APP应用查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:apps:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('APP应用新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:apps:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('APP应用修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:apps:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('APP应用删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:apps:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('APP应用导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:apps:export',       '#', 'admin', sysdate(), '', null, '');