-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('打包历史记录', '3', '1', 'pack_user_history', 'pack/pack_user_history/index', 1, 0, 'C', '0', '0', 'pack:pack_user_history:list', '#', 'admin', sysdate(), '', null, '打包历史记录菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('打包历史记录查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_history:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('打包历史记录新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_history:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('打包历史记录修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_history:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('打包历史记录删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_history:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('打包历史记录导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'pack:pack_user_history:export',       '#', 'admin', sysdate(), '', null, '');