package com.ruoyi.web.controller.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AppModuleItem;
import com.ruoyi.system.service.IAppModuleItemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 模块配置项Controller
 *
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/module/module_item")
public class AppModuleItemController extends BaseController
{
    @Autowired
    private IAppModuleItemService appModuleItemService;

    /**
     * 查询模块配置项列表
     */
    @PreAuthorize("@ss.hasPermi('module:module_item:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppModuleItem appModuleItem)
    {
        startPage();
        List<AppModuleItem> list = appModuleItemService.selectAppModuleItemList(appModuleItem);
        return getDataTable(list);
    }

    /**
     * 导出模块配置项列表
     */
    @PreAuthorize("@ss.hasPermi('module:module_item:export')")
    @Log(title = "模块配置项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppModuleItem appModuleItem)
    {
        List<AppModuleItem> list = appModuleItemService.selectAppModuleItemList(appModuleItem);
        ExcelUtil<AppModuleItem> util = new ExcelUtil<AppModuleItem>(AppModuleItem.class);
        util.exportExcel(response, list, "模块配置项数据");
    }

    /**
     * 获取模块配置项详细信息
     */
    @PreAuthorize("@ss.hasPermi('module:module_item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(appModuleItemService.selectAppModuleItemById(id));
    }

    /**
     * 新增模块配置项
     */
    @PreAuthorize("@ss.hasPermi('module:module_item:add')")
    @Log(title = "模块配置项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppModuleItem appModuleItem)
    {
        return toAjax(appModuleItemService.insertAppModuleItem(appModuleItem));
    }

    /**
     * 修改模块配置项
     */
    @PreAuthorize("@ss.hasPermi('module:module_item:edit')")
    @Log(title = "模块配置项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppModuleItem appModuleItem)
    {
        return toAjax(appModuleItemService.updateAppModuleItem(appModuleItem));
    }

    /**
     * 删除模块配置项
     */
    @PreAuthorize("@ss.hasPermi('module:module_item:remove')")
    @Log(title = "模块配置项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appModuleItemService.deleteAppModuleItemByIds(ids));
    }
}
