package com.ruoyi.web.controller.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AppModuleAdminUser;
import com.ruoyi.system.service.IAppModuleAdminUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户-APP模块配置项（管理用户的各项配置）Controller
 *
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/module/module_user")
public class AppModuleAdminUserController extends BaseController
{
    @Autowired
    private IAppModuleAdminUserService appModuleAdminUserService;

    /**
     * 查询用户-APP模块配置项（管理用户的各项配置）列表
     */
    @PreAuthorize("@ss.hasPermi('module:module_user:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppModuleAdminUser appModuleAdminUser)
    {
        startPage();
        List<AppModuleAdminUser> list = appModuleAdminUserService.selectAppModuleAdminUserList(appModuleAdminUser);
        return getDataTable(list);
    }

    /**
     * 导出用户-APP模块配置项（管理用户的各项配置）列表
     */
    @PreAuthorize("@ss.hasPermi('module:module_user:export')")
    @Log(title = "用户-APP模块配置项（管理用户的各项配置）", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppModuleAdminUser appModuleAdminUser)
    {
        List<AppModuleAdminUser> list = appModuleAdminUserService.selectAppModuleAdminUserList(appModuleAdminUser);
        ExcelUtil<AppModuleAdminUser> util = new ExcelUtil<AppModuleAdminUser>(AppModuleAdminUser.class);
        util.exportExcel(response, list, "用户-APP模块配置项（管理用户的各项配置）数据");
    }

    /**
     * 获取用户-APP模块配置项（管理用户的各项配置）详细信息
     */
    @PreAuthorize("@ss.hasPermi('module:module_user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(appModuleAdminUserService.selectAppModuleAdminUserById(id));
    }

    /**
     * 新增用户-APP模块配置项（管理用户的各项配置）
     */
    @PreAuthorize("@ss.hasPermi('module:module_user:add')")
    @Log(title = "用户-APP模块配置项（管理用户的各项配置）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppModuleAdminUser appModuleAdminUser)
    {
        return toAjax(appModuleAdminUserService.insertAppModuleAdminUser(appModuleAdminUser));
    }

    /**
     * 修改用户-APP模块配置项（管理用户的各项配置）
     */
    @PreAuthorize("@ss.hasPermi('module:module_user:edit')")
    @Log(title = "用户-APP模块配置项（管理用户的各项配置）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppModuleAdminUser appModuleAdminUser)
    {
        return toAjax(appModuleAdminUserService.updateAppModuleAdminUser(appModuleAdminUser));
    }

    /**
     * 删除用户-APP模块配置项（管理用户的各项配置）
     */
    @PreAuthorize("@ss.hasPermi('module:module_user:remove')")
    @Log(title = "用户-APP模块配置项（管理用户的各项配置）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appModuleAdminUserService.deleteAppModuleAdminUserByIds(ids));
    }
}
