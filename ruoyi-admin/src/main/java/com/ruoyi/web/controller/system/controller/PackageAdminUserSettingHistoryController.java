package com.ruoyi.web.controller.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PackageAdminUserSettingHistory;
import com.ruoyi.system.service.IPackageAdminUserSettingHistoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打包历史记录Controller
 *
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/pack/pack_user_history")
public class PackageAdminUserSettingHistoryController extends BaseController
{
    @Autowired
    private IPackageAdminUserSettingHistoryService packageAdminUserSettingHistoryService;

    /**
     * 查询打包历史记录列表
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_history:list')")
    @GetMapping("/list")
    public TableDataInfo list(PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        startPage();
        List<PackageAdminUserSettingHistory> list = packageAdminUserSettingHistoryService.selectPackageAdminUserSettingHistoryList(packageAdminUserSettingHistory);
        return getDataTable(list);
    }

    /**
     * 导出打包历史记录列表
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_history:export')")
    @Log(title = "打包历史记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        List<PackageAdminUserSettingHistory> list = packageAdminUserSettingHistoryService.selectPackageAdminUserSettingHistoryList(packageAdminUserSettingHistory);
        ExcelUtil<PackageAdminUserSettingHistory> util = new ExcelUtil<PackageAdminUserSettingHistory>(PackageAdminUserSettingHistory.class);
        util.exportExcel(response, list, "打包历史记录数据");
    }

    /**
     * 获取打包历史记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_history:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(packageAdminUserSettingHistoryService.selectPackageAdminUserSettingHistoryById(id));
    }

    /**
     * 新增打包历史记录
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_history:add')")
    @Log(title = "打包历史记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        return toAjax(packageAdminUserSettingHistoryService.insertPackageAdminUserSettingHistory(packageAdminUserSettingHistory));
    }

    /**
     * 修改打包历史记录
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_history:edit')")
    @Log(title = "打包历史记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        return toAjax(packageAdminUserSettingHistoryService.updatePackageAdminUserSettingHistory(packageAdminUserSettingHistory));
    }

    /**
     * 删除打包历史记录
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_history:remove')")
    @Log(title = "打包历史记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(packageAdminUserSettingHistoryService.deletePackageAdminUserSettingHistoryByIds(ids));
    }
}
