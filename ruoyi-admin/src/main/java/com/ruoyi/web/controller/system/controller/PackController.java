package com.ruoyi.web.controller.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.IPackageAdminUserSettingCustomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户打包设置Controller
 *
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/pack/pack_user_setting")
public class PackController extends BaseController {
    @Autowired
    private IPackageAdminUserSettingCustomService packageAdminUserSettingCustomService;

    /**
     * 获得打包配置信息
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:edit')")
    @Log(title = "获得打包配置信息", businessType = BusinessType.OTHER)
    @PostMapping("/sitemap/{id}")
    public AjaxResult sitemap(@PathVariable Long id) {
        return toAjax(packageAdminUserSettingCustomService.sitemap(id));
    }

    /**
     * 开始打包
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:edit')")
    @Log(title = "开始打包", businessType = BusinessType.OTHER)
    @PostMapping("/build/{id}")
    public AjaxResult build(@PathVariable Long id) {
        return toAjax(packageAdminUserSettingCustomService.buildPackage(id));
    }
}
