package com.ruoyi.web.controller.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Apps;
import com.ruoyi.system.service.IAppsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * APP应用Controller
 *
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/system/apps")
public class AppsController extends BaseController
{
    @Autowired
    private IAppsService appsService;

    /**
     * 查询APP应用列表
     */
    @PreAuthorize("@ss.hasPermi('system:apps:list')")
    @GetMapping("/list")
    public TableDataInfo list(Apps apps)
    {
        startPage();
        List<Apps> list = appsService.selectAppsList(apps);
        return getDataTable(list);
    }

    /**
     * 导出APP应用列表
     */
    @PreAuthorize("@ss.hasPermi('system:apps:export')")
    @Log(title = "APP应用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Apps apps)
    {
        List<Apps> list = appsService.selectAppsList(apps);
        ExcelUtil<Apps> util = new ExcelUtil<Apps>(Apps.class);
        util.exportExcel(response, list, "APP应用数据");
    }

    /**
     * 获取APP应用详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:apps:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(appsService.selectAppsById(id));
    }

    /**
     * 新增APP应用
     */
    @PreAuthorize("@ss.hasPermi('system:apps:add')")
    @Log(title = "APP应用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Apps apps)
    {
        return toAjax(appsService.insertApps(apps));
    }

    /**
     * 修改APP应用
     */
    @PreAuthorize("@ss.hasPermi('system:apps:edit')")
    @Log(title = "APP应用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Apps apps)
    {
        return toAjax(appsService.updateApps(apps));
    }

    /**
     * 删除APP应用
     */
    @PreAuthorize("@ss.hasPermi('system:apps:remove')")
    @Log(title = "APP应用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appsService.deleteAppsByIds(ids));
    }
}
