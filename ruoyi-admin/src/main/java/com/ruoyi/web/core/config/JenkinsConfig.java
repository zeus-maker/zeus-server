package com.ruoyi.web.core.config;

import com.ruoyi.system.util.jenkins.JobApi;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JenkinsConfig {
    @Bean
    @ConditionalOnMissingBean
    public JobApi jobApi() {
        return new JobApi();
    }
}
