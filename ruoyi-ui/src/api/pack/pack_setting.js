import request from '@/utils/request'

// 查询打包配置列表
export function listPack_setting(query) {
  return request({
    url: '/pack/pack_setting/list',
    method: 'get',
    params: query
  })
}

// 查询打包配置详细
export function getPack_setting(id) {
  return request({
    url: '/pack/pack_setting/' + id,
    method: 'get'
  })
}

// 新增打包配置
export function addPack_setting(data) {
  return request({
    url: '/pack/pack_setting',
    method: 'post',
    data: data
  })
}

// 修改打包配置
export function updatePack_setting(data) {
  return request({
    url: '/pack/pack_setting',
    method: 'put',
    data: data
  })
}

// 删除打包配置
export function delPack_setting(id) {
  return request({
    url: '/pack/pack_setting/' + id,
    method: 'delete'
  })
}
