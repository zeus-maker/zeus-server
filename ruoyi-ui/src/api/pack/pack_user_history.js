import request from '@/utils/request'

// 查询打包历史记录列表
export function listPack_user_history(query) {
  return request({
    url: '/pack/pack_user_history/list',
    method: 'get',
    params: query
  })
}

// 查询打包历史记录详细
export function getPack_user_history(id) {
  return request({
    url: '/pack/pack_user_history/' + id,
    method: 'get'
  })
}

// 新增打包历史记录
export function addPack_user_history(data) {
  return request({
    url: '/pack/pack_user_history',
    method: 'post',
    data: data
  })
}

// 修改打包历史记录
export function updatePack_user_history(data) {
  return request({
    url: '/pack/pack_user_history',
    method: 'put',
    data: data
  })
}

// 删除打包历史记录
export function delPack_user_history(id) {
  return request({
    url: '/pack/pack_user_history/' + id,
    method: 'delete'
  })
}
