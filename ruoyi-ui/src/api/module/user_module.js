import request from '@/utils/request'

// 查询用户模块配置列表
export function listUser_module(query) {
  return request({
    url: '/module/user_module/list',
    method: 'get',
    params: query
  })
}

// 查询用户模块配置详细
export function getUser_module(id) {
  return request({
    url: '/module/user_module/' + id,
    method: 'get'
  })
}

// 新增用户模块配置
export function addUser_module(data) {
  return request({
    url: '/module/user_module',
    method: 'post',
    data: data
  })
}

// 修改用户模块配置
export function updateUser_module(data) {
  return request({
    url: '/module/user_module',
    method: 'put',
    data: data
  })
}

// 删除用户模块配置
export function delUser_module(id) {
  return request({
    url: '/module/user_module/' + id,
    method: 'delete'
  })
}
