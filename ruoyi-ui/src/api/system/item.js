import request from '@/utils/request'

// 查询 APP组件-&gt;APP组件配置项列表
export function listItem(query) {
  return request({
    url: '/system/item/list',
    method: 'get',
    params: query
  })
}

// 查询 APP组件-&gt;APP组件配置项详细
export function getItem(id) {
  return request({
    url: '/system/item/' + id,
    method: 'get'
  })
}

// 新增 APP组件-&gt;APP组件配置项
export function addItem(data) {
  return request({
    url: '/system/item',
    method: 'post',
    data: data
  })
}

// 修改 APP组件-&gt;APP组件配置项
export function updateItem(data) {
  return request({
    url: '/system/item',
    method: 'put',
    data: data
  })
}

// 删除 APP组件-&gt;APP组件配置项
export function delItem(id) {
  return request({
    url: '/system/item/' + id,
    method: 'delete'
  })
}
