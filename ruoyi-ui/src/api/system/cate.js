import request from '@/utils/request'

// 查询APP组件分类列表
export function listCate(query) {
  return request({
    url: '/system/cate/list',
    method: 'get',
    params: query
  })
}

// 查询APP组件分类详细
export function getCate(id) {
  return request({
    url: '/system/cate/' + id,
    method: 'get'
  })
}

// 新增APP组件分类
export function addCate(data) {
  return request({
    url: '/system/cate',
    method: 'post',
    data: data
  })
}

// 修改APP组件分类
export function updateCate(data) {
  return request({
    url: '/system/cate',
    method: 'put',
    data: data
  })
}

// 删除APP组件分类
export function delCate(id) {
  return request({
    url: '/system/cate/' + id,
    method: 'delete'
  })
}
