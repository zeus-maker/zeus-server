import request from '@/utils/request'

// 查询模块配置项列表
export function listModuleItem(query) {
  return request({
    url: '/system/moduleItem/list',
    method: 'get',
    params: query
  })
}

// 查询模块配置项详细
export function getModuleItem(id) {
  return request({
    url: '/system/moduleItem/' + id,
    method: 'get'
  })
}

// 新增模块配置项
export function addModuleItem(data) {
  return request({
    url: '/system/moduleItem',
    method: 'post',
    data: data
  })
}

// 修改模块配置项
export function updateModuleItem(data) {
  return request({
    url: '/system/moduleItem',
    method: 'put',
    data: data
  })
}

// 删除模块配置项
export function delModuleItem(id) {
  return request({
    url: '/system/moduleItem/' + id,
    method: 'delete'
  })
}
