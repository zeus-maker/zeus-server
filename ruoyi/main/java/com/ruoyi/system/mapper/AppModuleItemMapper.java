package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AppModuleItem;

/**
 * 模块配置项Mapper接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface AppModuleItemMapper 
{
    /**
     * 查询模块配置项
     * 
     * @param id 模块配置项主键
     * @return 模块配置项
     */
    public AppModuleItem selectAppModuleItemById(Long id);

    /**
     * 查询模块配置项列表
     * 
     * @param appModuleItem 模块配置项
     * @return 模块配置项集合
     */
    public List<AppModuleItem> selectAppModuleItemList(AppModuleItem appModuleItem);

    /**
     * 新增模块配置项
     * 
     * @param appModuleItem 模块配置项
     * @return 结果
     */
    public int insertAppModuleItem(AppModuleItem appModuleItem);

    /**
     * 修改模块配置项
     * 
     * @param appModuleItem 模块配置项
     * @return 结果
     */
    public int updateAppModuleItem(AppModuleItem appModuleItem);

    /**
     * 删除模块配置项
     * 
     * @param id 模块配置项主键
     * @return 结果
     */
    public int deleteAppModuleItemById(Long id);

    /**
     * 批量删除模块配置项
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAppModuleItemByIds(Long[] ids);
}
