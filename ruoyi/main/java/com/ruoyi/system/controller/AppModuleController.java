package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AppModule;
import com.ruoyi.system.service.IAppModuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 模块配置Controller
 * 
 * @author marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/module/module")
public class AppModuleController extends BaseController
{
    @Autowired
    private IAppModuleService appModuleService;

    /**
     * 查询模块配置列表
     */
    @PreAuthorize("@ss.hasPermi('module:module:list')")
    @GetMapping("/list")
    public AjaxResult list(AppModule appModule)
    {
        List<AppModule> list = appModuleService.selectAppModuleList(appModule);
        return success(list);
    }

    /**
     * 导出模块配置列表
     */
    @PreAuthorize("@ss.hasPermi('module:module:export')")
    @Log(title = "模块配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppModule appModule)
    {
        List<AppModule> list = appModuleService.selectAppModuleList(appModule);
        ExcelUtil<AppModule> util = new ExcelUtil<AppModule>(AppModule.class);
        util.exportExcel(response, list, "模块配置数据");
    }

    /**
     * 获取模块配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('module:module:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(appModuleService.selectAppModuleById(id));
    }

    /**
     * 新增模块配置
     */
    @PreAuthorize("@ss.hasPermi('module:module:add')")
    @Log(title = "模块配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppModule appModule)
    {
        return toAjax(appModuleService.insertAppModule(appModule));
    }

    /**
     * 修改模块配置
     */
    @PreAuthorize("@ss.hasPermi('module:module:edit')")
    @Log(title = "模块配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppModule appModule)
    {
        return toAjax(appModuleService.updateAppModule(appModule));
    }

    /**
     * 删除模块配置
     */
    @PreAuthorize("@ss.hasPermi('module:module:remove')")
    @Log(title = "模块配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appModuleService.deleteAppModuleByIds(ids));
    }
}
