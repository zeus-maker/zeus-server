package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PackageAdminUserSetting;
import com.ruoyi.system.service.IPackageAdminUserSettingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户打包设置Controller
 * 
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/pack/pack_user_setting")
public class PackageAdminUserSettingController extends BaseController
{
    @Autowired
    private IPackageAdminUserSettingService packageAdminUserSettingService;

    /**
     * 查询用户打包设置列表
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:list')")
    @GetMapping("/list")
    public TableDataInfo list(PackageAdminUserSetting packageAdminUserSetting)
    {
        startPage();
        List<PackageAdminUserSetting> list = packageAdminUserSettingService.selectPackageAdminUserSettingList(packageAdminUserSetting);
        return getDataTable(list);
    }

    /**
     * 导出用户打包设置列表
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:export')")
    @Log(title = "用户打包设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PackageAdminUserSetting packageAdminUserSetting)
    {
        List<PackageAdminUserSetting> list = packageAdminUserSettingService.selectPackageAdminUserSettingList(packageAdminUserSetting);
        ExcelUtil<PackageAdminUserSetting> util = new ExcelUtil<PackageAdminUserSetting>(PackageAdminUserSetting.class);
        util.exportExcel(response, list, "用户打包设置数据");
    }

    /**
     * 获取用户打包设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(packageAdminUserSettingService.selectPackageAdminUserSettingById(id));
    }

    /**
     * 新增用户打包设置
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:add')")
    @Log(title = "用户打包设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PackageAdminUserSetting packageAdminUserSetting)
    {
        return toAjax(packageAdminUserSettingService.insertPackageAdminUserSetting(packageAdminUserSetting));
    }

    /**
     * 修改用户打包设置
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:edit')")
    @Log(title = "用户打包设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PackageAdminUserSetting packageAdminUserSetting)
    {
        return toAjax(packageAdminUserSettingService.updatePackageAdminUserSetting(packageAdminUserSetting));
    }

    /**
     * 删除用户打包设置
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_user_setting:remove')")
    @Log(title = "用户打包设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(packageAdminUserSettingService.deletePackageAdminUserSettingByIds(ids));
    }
}
