package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PackageSettingFile;
import com.ruoyi.system.service.IPackageSettingFileService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打包配置文件Controller
 * 
 * @author Marion
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/pack/pack_setting_file")
public class PackageSettingFileController extends BaseController
{
    @Autowired
    private IPackageSettingFileService packageSettingFileService;

    /**
     * 查询打包配置文件列表
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_setting_file:list')")
    @GetMapping("/list")
    public TableDataInfo list(PackageSettingFile packageSettingFile)
    {
        startPage();
        List<PackageSettingFile> list = packageSettingFileService.selectPackageSettingFileList(packageSettingFile);
        return getDataTable(list);
    }

    /**
     * 导出打包配置文件列表
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_setting_file:export')")
    @Log(title = "打包配置文件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PackageSettingFile packageSettingFile)
    {
        List<PackageSettingFile> list = packageSettingFileService.selectPackageSettingFileList(packageSettingFile);
        ExcelUtil<PackageSettingFile> util = new ExcelUtil<PackageSettingFile>(PackageSettingFile.class);
        util.exportExcel(response, list, "打包配置文件数据");
    }

    /**
     * 获取打包配置文件详细信息
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_setting_file:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(packageSettingFileService.selectPackageSettingFileById(id));
    }

    /**
     * 新增打包配置文件
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_setting_file:add')")
    @Log(title = "打包配置文件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PackageSettingFile packageSettingFile)
    {
        return toAjax(packageSettingFileService.insertPackageSettingFile(packageSettingFile));
    }

    /**
     * 修改打包配置文件
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_setting_file:edit')")
    @Log(title = "打包配置文件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PackageSettingFile packageSettingFile)
    {
        return toAjax(packageSettingFileService.updatePackageSettingFile(packageSettingFile));
    }

    /**
     * 删除打包配置文件
     */
    @PreAuthorize("@ss.hasPermi('pack:pack_setting_file:remove')")
    @Log(title = "打包配置文件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(packageSettingFileService.deletePackageSettingFileByIds(ids));
    }
}
