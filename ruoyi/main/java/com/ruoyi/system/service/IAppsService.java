package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Apps;

/**
 * APP应用Service接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface IAppsService 
{
    /**
     * 查询APP应用
     * 
     * @param id APP应用主键
     * @return APP应用
     */
    public Apps selectAppsById(Long id);

    /**
     * 查询APP应用列表
     * 
     * @param apps APP应用
     * @return APP应用集合
     */
    public List<Apps> selectAppsList(Apps apps);

    /**
     * 新增APP应用
     * 
     * @param apps APP应用
     * @return 结果
     */
    public int insertApps(Apps apps);

    /**
     * 修改APP应用
     * 
     * @param apps APP应用
     * @return 结果
     */
    public int updateApps(Apps apps);

    /**
     * 批量删除APP应用
     * 
     * @param ids 需要删除的APP应用主键集合
     * @return 结果
     */
    public int deleteAppsByIds(Long[] ids);

    /**
     * 删除APP应用信息
     * 
     * @param id APP应用主键
     * @return 结果
     */
    public int deleteAppsById(Long id);
}
