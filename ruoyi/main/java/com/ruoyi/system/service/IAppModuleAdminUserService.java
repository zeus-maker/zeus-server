package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AppModuleAdminUser;

/**
 * 用户-APP模块配置项（管理用户的各项配置）Service接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface IAppModuleAdminUserService 
{
    /**
     * 查询用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param id 用户-APP模块配置项（管理用户的各项配置）主键
     * @return 用户-APP模块配置项（管理用户的各项配置）
     */
    public AppModuleAdminUser selectAppModuleAdminUserById(Long id);

    /**
     * 查询用户-APP模块配置项（管理用户的各项配置）列表
     * 
     * @param appModuleAdminUser 用户-APP模块配置项（管理用户的各项配置）
     * @return 用户-APP模块配置项（管理用户的各项配置）集合
     */
    public List<AppModuleAdminUser> selectAppModuleAdminUserList(AppModuleAdminUser appModuleAdminUser);

    /**
     * 新增用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param appModuleAdminUser 用户-APP模块配置项（管理用户的各项配置）
     * @return 结果
     */
    public int insertAppModuleAdminUser(AppModuleAdminUser appModuleAdminUser);

    /**
     * 修改用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param appModuleAdminUser 用户-APP模块配置项（管理用户的各项配置）
     * @return 结果
     */
    public int updateAppModuleAdminUser(AppModuleAdminUser appModuleAdminUser);

    /**
     * 批量删除用户-APP模块配置项（管理用户的各项配置）
     * 
     * @param ids 需要删除的用户-APP模块配置项（管理用户的各项配置）主键集合
     * @return 结果
     */
    public int deleteAppModuleAdminUserByIds(Long[] ids);

    /**
     * 删除用户-APP模块配置项（管理用户的各项配置）信息
     * 
     * @param id 用户-APP模块配置项（管理用户的各项配置）主键
     * @return 结果
     */
    public int deleteAppModuleAdminUserById(Long id);
}
