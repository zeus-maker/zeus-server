package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PackageAdminUserSettingMapper;
import com.ruoyi.system.domain.PackageAdminUserSetting;
import com.ruoyi.system.service.IPackageAdminUserSettingService;

/**
 * 用户打包设置Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class PackageAdminUserSettingServiceImpl implements IPackageAdminUserSettingService 
{
    @Autowired
    private PackageAdminUserSettingMapper packageAdminUserSettingMapper;

    /**
     * 查询用户打包设置
     * 
     * @param id 用户打包设置主键
     * @return 用户打包设置
     */
    @Override
    public PackageAdminUserSetting selectPackageAdminUserSettingById(Long id)
    {
        return packageAdminUserSettingMapper.selectPackageAdminUserSettingById(id);
    }

    /**
     * 查询用户打包设置列表
     * 
     * @param packageAdminUserSetting 用户打包设置
     * @return 用户打包设置
     */
    @Override
    public List<PackageAdminUserSetting> selectPackageAdminUserSettingList(PackageAdminUserSetting packageAdminUserSetting)
    {
        return packageAdminUserSettingMapper.selectPackageAdminUserSettingList(packageAdminUserSetting);
    }

    /**
     * 新增用户打包设置
     * 
     * @param packageAdminUserSetting 用户打包设置
     * @return 结果
     */
    @Override
    public int insertPackageAdminUserSetting(PackageAdminUserSetting packageAdminUserSetting)
    {
        packageAdminUserSetting.setCreateTime(DateUtils.getNowDate());
        return packageAdminUserSettingMapper.insertPackageAdminUserSetting(packageAdminUserSetting);
    }

    /**
     * 修改用户打包设置
     * 
     * @param packageAdminUserSetting 用户打包设置
     * @return 结果
     */
    @Override
    public int updatePackageAdminUserSetting(PackageAdminUserSetting packageAdminUserSetting)
    {
        packageAdminUserSetting.setUpdateTime(DateUtils.getNowDate());
        return packageAdminUserSettingMapper.updatePackageAdminUserSetting(packageAdminUserSetting);
    }

    /**
     * 批量删除用户打包设置
     * 
     * @param ids 需要删除的用户打包设置主键
     * @return 结果
     */
    @Override
    public int deletePackageAdminUserSettingByIds(Long[] ids)
    {
        return packageAdminUserSettingMapper.deletePackageAdminUserSettingByIds(ids);
    }

    /**
     * 删除用户打包设置信息
     * 
     * @param id 用户打包设置主键
     * @return 结果
     */
    @Override
    public int deletePackageAdminUserSettingById(Long id)
    {
        return packageAdminUserSettingMapper.deletePackageAdminUserSettingById(id);
    }
}
