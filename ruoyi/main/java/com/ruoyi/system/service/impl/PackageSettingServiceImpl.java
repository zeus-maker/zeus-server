package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PackageSettingMapper;
import com.ruoyi.system.domain.PackageSetting;
import com.ruoyi.system.service.IPackageSettingService;

/**
 * 打包配置Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class PackageSettingServiceImpl implements IPackageSettingService 
{
    @Autowired
    private PackageSettingMapper packageSettingMapper;

    /**
     * 查询打包配置
     * 
     * @param id 打包配置主键
     * @return 打包配置
     */
    @Override
    public PackageSetting selectPackageSettingById(Long id)
    {
        return packageSettingMapper.selectPackageSettingById(id);
    }

    /**
     * 查询打包配置列表
     * 
     * @param packageSetting 打包配置
     * @return 打包配置
     */
    @Override
    public List<PackageSetting> selectPackageSettingList(PackageSetting packageSetting)
    {
        return packageSettingMapper.selectPackageSettingList(packageSetting);
    }

    /**
     * 新增打包配置
     * 
     * @param packageSetting 打包配置
     * @return 结果
     */
    @Override
    public int insertPackageSetting(PackageSetting packageSetting)
    {
        packageSetting.setCreateTime(DateUtils.getNowDate());
        return packageSettingMapper.insertPackageSetting(packageSetting);
    }

    /**
     * 修改打包配置
     * 
     * @param packageSetting 打包配置
     * @return 结果
     */
    @Override
    public int updatePackageSetting(PackageSetting packageSetting)
    {
        packageSetting.setUpdateTime(DateUtils.getNowDate());
        return packageSettingMapper.updatePackageSetting(packageSetting);
    }

    /**
     * 批量删除打包配置
     * 
     * @param ids 需要删除的打包配置主键
     * @return 结果
     */
    @Override
    public int deletePackageSettingByIds(Long[] ids)
    {
        return packageSettingMapper.deletePackageSettingByIds(ids);
    }

    /**
     * 删除打包配置信息
     * 
     * @param id 打包配置主键
     * @return 结果
     */
    @Override
    public int deletePackageSettingById(Long id)
    {
        return packageSettingMapper.deletePackageSettingById(id);
    }
}
