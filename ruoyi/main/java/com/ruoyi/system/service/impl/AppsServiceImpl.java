package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AppsMapper;
import com.ruoyi.system.domain.Apps;
import com.ruoyi.system.service.IAppsService;

/**
 * APP应用Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class AppsServiceImpl implements IAppsService 
{
    @Autowired
    private AppsMapper appsMapper;

    /**
     * 查询APP应用
     * 
     * @param id APP应用主键
     * @return APP应用
     */
    @Override
    public Apps selectAppsById(Long id)
    {
        return appsMapper.selectAppsById(id);
    }

    /**
     * 查询APP应用列表
     * 
     * @param apps APP应用
     * @return APP应用
     */
    @Override
    public List<Apps> selectAppsList(Apps apps)
    {
        return appsMapper.selectAppsList(apps);
    }

    /**
     * 新增APP应用
     * 
     * @param apps APP应用
     * @return 结果
     */
    @Override
    public int insertApps(Apps apps)
    {
        apps.setCreateTime(DateUtils.getNowDate());
        return appsMapper.insertApps(apps);
    }

    /**
     * 修改APP应用
     * 
     * @param apps APP应用
     * @return 结果
     */
    @Override
    public int updateApps(Apps apps)
    {
        apps.setUpdateTime(DateUtils.getNowDate());
        return appsMapper.updateApps(apps);
    }

    /**
     * 批量删除APP应用
     * 
     * @param ids 需要删除的APP应用主键
     * @return 结果
     */
    @Override
    public int deleteAppsByIds(Long[] ids)
    {
        return appsMapper.deleteAppsByIds(ids);
    }

    /**
     * 删除APP应用信息
     * 
     * @param id APP应用主键
     * @return 结果
     */
    @Override
    public int deleteAppsById(Long id)
    {
        return appsMapper.deleteAppsById(id);
    }
}
