package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PackageAdminUserSettingHistoryMapper;
import com.ruoyi.system.domain.PackageAdminUserSettingHistory;
import com.ruoyi.system.service.IPackageAdminUserSettingHistoryService;

/**
 * 打包历史记录Service业务层处理
 * 
 * @author Marion
 * @date 2023-07-12
 */
@Service
public class PackageAdminUserSettingHistoryServiceImpl implements IPackageAdminUserSettingHistoryService 
{
    @Autowired
    private PackageAdminUserSettingHistoryMapper packageAdminUserSettingHistoryMapper;

    /**
     * 查询打包历史记录
     * 
     * @param id 打包历史记录主键
     * @return 打包历史记录
     */
    @Override
    public PackageAdminUserSettingHistory selectPackageAdminUserSettingHistoryById(Long id)
    {
        return packageAdminUserSettingHistoryMapper.selectPackageAdminUserSettingHistoryById(id);
    }

    /**
     * 查询打包历史记录列表
     * 
     * @param packageAdminUserSettingHistory 打包历史记录
     * @return 打包历史记录
     */
    @Override
    public List<PackageAdminUserSettingHistory> selectPackageAdminUserSettingHistoryList(PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        return packageAdminUserSettingHistoryMapper.selectPackageAdminUserSettingHistoryList(packageAdminUserSettingHistory);
    }

    /**
     * 新增打包历史记录
     * 
     * @param packageAdminUserSettingHistory 打包历史记录
     * @return 结果
     */
    @Override
    public int insertPackageAdminUserSettingHistory(PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        packageAdminUserSettingHistory.setCreateTime(DateUtils.getNowDate());
        return packageAdminUserSettingHistoryMapper.insertPackageAdminUserSettingHistory(packageAdminUserSettingHistory);
    }

    /**
     * 修改打包历史记录
     * 
     * @param packageAdminUserSettingHistory 打包历史记录
     * @return 结果
     */
    @Override
    public int updatePackageAdminUserSettingHistory(PackageAdminUserSettingHistory packageAdminUserSettingHistory)
    {
        packageAdminUserSettingHistory.setUpdateTime(DateUtils.getNowDate());
        return packageAdminUserSettingHistoryMapper.updatePackageAdminUserSettingHistory(packageAdminUserSettingHistory);
    }

    /**
     * 批量删除打包历史记录
     * 
     * @param ids 需要删除的打包历史记录主键
     * @return 结果
     */
    @Override
    public int deletePackageAdminUserSettingHistoryByIds(Long[] ids)
    {
        return packageAdminUserSettingHistoryMapper.deletePackageAdminUserSettingHistoryByIds(ids);
    }

    /**
     * 删除打包历史记录信息
     * 
     * @param id 打包历史记录主键
     * @return 结果
     */
    @Override
    public int deletePackageAdminUserSettingHistoryById(Long id)
    {
        return packageAdminUserSettingHistoryMapper.deletePackageAdminUserSettingHistoryById(id);
    }
}
