package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PackageAdminUserSetting;

/**
 * 用户打包设置Service接口
 * 
 * @author Marion
 * @date 2023-07-12
 */
public interface IPackageAdminUserSettingService 
{
    /**
     * 查询用户打包设置
     * 
     * @param id 用户打包设置主键
     * @return 用户打包设置
     */
    public PackageAdminUserSetting selectPackageAdminUserSettingById(Long id);

    /**
     * 查询用户打包设置列表
     * 
     * @param packageAdminUserSetting 用户打包设置
     * @return 用户打包设置集合
     */
    public List<PackageAdminUserSetting> selectPackageAdminUserSettingList(PackageAdminUserSetting packageAdminUserSetting);

    /**
     * 新增用户打包设置
     * 
     * @param packageAdminUserSetting 用户打包设置
     * @return 结果
     */
    public int insertPackageAdminUserSetting(PackageAdminUserSetting packageAdminUserSetting);

    /**
     * 修改用户打包设置
     * 
     * @param packageAdminUserSetting 用户打包设置
     * @return 结果
     */
    public int updatePackageAdminUserSetting(PackageAdminUserSetting packageAdminUserSetting);

    /**
     * 批量删除用户打包设置
     * 
     * @param ids 需要删除的用户打包设置主键集合
     * @return 结果
     */
    public int deletePackageAdminUserSettingByIds(Long[] ids);

    /**
     * 删除用户打包设置信息
     * 
     * @param id 用户打包设置主键
     * @return 结果
     */
    public int deletePackageAdminUserSettingById(Long id);
}
