import request from '@/utils/request'

// 查询APP应用列表
export function listApps(query) {
  return request({
    url: '/system/apps/list',
    method: 'get',
    params: query
  })
}

// 查询APP应用详细
export function getApps(id) {
  return request({
    url: '/system/apps/' + id,
    method: 'get'
  })
}

// 新增APP应用
export function addApps(data) {
  return request({
    url: '/system/apps',
    method: 'post',
    data: data
  })
}

// 修改APP应用
export function updateApps(data) {
  return request({
    url: '/system/apps',
    method: 'put',
    data: data
  })
}

// 删除APP应用
export function delApps(id) {
  return request({
    url: '/system/apps/' + id,
    method: 'delete'
  })
}
