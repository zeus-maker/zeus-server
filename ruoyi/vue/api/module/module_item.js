import request from '@/utils/request'

// 查询模块配置项列表
export function listModule_item(query) {
  return request({
    url: '/module/module_item/list',
    method: 'get',
    params: query
  })
}

// 查询模块配置项详细
export function getModule_item(id) {
  return request({
    url: '/module/module_item/' + id,
    method: 'get'
  })
}

// 新增模块配置项
export function addModule_item(data) {
  return request({
    url: '/module/module_item',
    method: 'post',
    data: data
  })
}

// 修改模块配置项
export function updateModule_item(data) {
  return request({
    url: '/module/module_item',
    method: 'put',
    data: data
  })
}

// 删除模块配置项
export function delModule_item(id) {
  return request({
    url: '/module/module_item/' + id,
    method: 'delete'
  })
}
