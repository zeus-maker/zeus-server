import request from '@/utils/request'

// 查询模块配置列表
export function listModule(query) {
  return request({
    url: '/module/module/list',
    method: 'get',
    params: query
  })
}

// 查询模块配置详细
export function getModule(id) {
  return request({
    url: '/module/module/' + id,
    method: 'get'
  })
}

// 新增模块配置
export function addModule(data) {
  return request({
    url: '/module/module',
    method: 'post',
    data: data
  })
}

// 修改模块配置
export function updateModule(data) {
  return request({
    url: '/module/module',
    method: 'put',
    data: data
  })
}

// 删除模块配置
export function delModule(id) {
  return request({
    url: '/module/module/' + id,
    method: 'delete'
  })
}
