import request from '@/utils/request'

// 查询用户-APP模块配置项（管理用户的各项配置）列表
export function listModule_user(query) {
  return request({
    url: '/module/module_user/list',
    method: 'get',
    params: query
  })
}

// 查询用户-APP模块配置项（管理用户的各项配置）详细
export function getModule_user(id) {
  return request({
    url: '/module/module_user/' + id,
    method: 'get'
  })
}

// 新增用户-APP模块配置项（管理用户的各项配置）
export function addModule_user(data) {
  return request({
    url: '/module/module_user',
    method: 'post',
    data: data
  })
}

// 修改用户-APP模块配置项（管理用户的各项配置）
export function updateModule_user(data) {
  return request({
    url: '/module/module_user',
    method: 'put',
    data: data
  })
}

// 删除用户-APP模块配置项（管理用户的各项配置）
export function delModule_user(id) {
  return request({
    url: '/module/module_user/' + id,
    method: 'delete'
  })
}
