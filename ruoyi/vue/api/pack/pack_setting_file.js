import request from '@/utils/request'

// 查询打包配置文件列表
export function listPack_setting_file(query) {
  return request({
    url: '/pack/pack_setting_file/list',
    method: 'get',
    params: query
  })
}

// 查询打包配置文件详细
export function getPack_setting_file(id) {
  return request({
    url: '/pack/pack_setting_file/' + id,
    method: 'get'
  })
}

// 新增打包配置文件
export function addPack_setting_file(data) {
  return request({
    url: '/pack/pack_setting_file',
    method: 'post',
    data: data
  })
}

// 修改打包配置文件
export function updatePack_setting_file(data) {
  return request({
    url: '/pack/pack_setting_file',
    method: 'put',
    data: data
  })
}

// 删除打包配置文件
export function delPack_setting_file(id) {
  return request({
    url: '/pack/pack_setting_file/' + id,
    method: 'delete'
  })
}
