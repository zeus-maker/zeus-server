import request from '@/utils/request'

// 查询用户打包设置列表
export function listPack_user_setting(query) {
  return request({
    url: '/pack/pack_user_setting/list',
    method: 'get',
    params: query
  })
}

// 查询用户打包设置详细
export function getPack_user_setting(id) {
  return request({
    url: '/pack/pack_user_setting/' + id,
    method: 'get'
  })
}

// 新增用户打包设置
export function addPack_user_setting(data) {
  return request({
    url: '/pack/pack_user_setting',
    method: 'post',
    data: data
  })
}

// 修改用户打包设置
export function updatePack_user_setting(data) {
  return request({
    url: '/pack/pack_user_setting',
    method: 'put',
    data: data
  })
}

// 删除用户打包设置
export function delPack_user_setting(id) {
  return request({
    url: '/pack/pack_user_setting/' + id,
    method: 'delete'
  })
}
