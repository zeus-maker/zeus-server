-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('模块配置项', '2027', '1', 'module_item', 'module/module_item/index', 1, 0, 'C', '0', '0', 'module:module_item:list', '#', 'admin', sysdate(), '', null, '模块配置项菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('模块配置项查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'module:module_item:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('模块配置项新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'module:module_item:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('模块配置项修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'module:module_item:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('模块配置项删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'module:module_item:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('模块配置项导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'module:module_item:export',       '#', 'admin', sysdate(), '', null, '');