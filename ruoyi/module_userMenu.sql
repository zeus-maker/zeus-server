-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户-APP模块配置项（管理用户的各项配置）', '3', '1', 'module_user', 'module/module_user/index', 1, 0, 'C', '0', '0', 'module:module_user:list', '#', 'admin', sysdate(), '', null, '用户-APP模块配置项（管理用户的各项配置）菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户-APP模块配置项（管理用户的各项配置）查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'module:module_user:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户-APP模块配置项（管理用户的各项配置）新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'module:module_user:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户-APP模块配置项（管理用户的各项配置）修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'module:module_user:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户-APP模块配置项（管理用户的各项配置）删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'module:module_user:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('用户-APP模块配置项（管理用户的各项配置）导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'module:module_user:export',       '#', 'admin', sysdate(), '', null, '');